# CHIP-8 and SuperCHIP-8 emulator project #


## School ##

Ecole des Mines de Nantes

## Students ##

Gabriel Queste (gqueste)

Gwenaël Provost

Quentin Victoor

##  Project ##

The main purpose of this project was to develop a CHIP-8 emulator during 2 SCRUM sprints of one week.

As a result the emulator is not finished, some SuperCHIP-8 functionnalities have not been implemented.

Recognized keys : 

1 2 3 4

A Z E R

Q S D F

W X C V